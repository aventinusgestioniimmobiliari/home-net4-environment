#!/bin/bash
# Source : http://wiki.clusterlabs.org/wiki/DRBD_PgSQL_HowTo
. ./main.yml

echo "Install PostgreSQL and make sure that you disabled automatic startup for it: "
systemctl status postgresql 
systemctl stop postgresql

echo "Initialize PostgreSQL database if necessary "
service postgresql initdb

echo "set up DRBD active/passive cluster with Pacemaker."
. ../templates/drbd-primary-scondary.sh

echo "add a shared IP to $CLUSTER-NAME"
crm configure primitive pgsqldbIP ocf:heartbeat:IPaddr params ip="${DRBD-SERVICE-IP}"

echo "ou also should copy all files from /var/lib/pgsql/data directory to that new filesystem"
echo ""
crm configure primitive dbFS ocf:heartbeat:Filesystem \
    params fstype="${CRM-FSTYPE}" directory="${DRBD-DEVICE-DIR}/pgsql/data" device="/dev/${DRBD-DEVICE-NAME}"

echo "adding PostgreSQL resource"
crm configure primitive pgsql-${PSQL-DB} ocf:heartbeat:pgsql

echo "combine all our resources into a group to be able to manage all of them as one"
crm configure group ${CLUSTER-RESOURCE-GROUP} pgsqldbIP  dbFS pgsql-${PSQL-DB}

echo "use ${DRBD-NODE1} as a primary node for our cluster: "

crm configure location primNode ${CLUSTER-RESOURCE-GROUP} rule "1000: #uname eq  ${DRBD-NODE1}"

echo "Configure ${CLUSTER-RESOURCE-GROUP} has to be collocated with DRBD master resource and start after DRBD started"
crm configure colocation ${CLUSTER-RESOURCE-GROUP}_on_master inf: ${CLUSTER-RESOURCE-GROUP} ms-${DRBD-DEVICE-NAME}:Master
crm configure order ${DRBD-DEVICE-NAME}_before_${CLUSTER-RESOURCE-GROUP} : ms-${DRBD-DEVICE-NAME}:promote dbGroup:start

echo "we can start our group"
crm resoource start ${CLUSTER-RESOURCE-GROUP}

echo "run crm_mon"

echo ""

echo ""

echo ""
