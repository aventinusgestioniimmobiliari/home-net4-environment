#!/bin/bash
. ./drbd/main.yml

crm configure primitive drbd_${DRBD-DEVICE-NAME} ocf:linbit:drbd \
        params drbd_resource="${DRBD-DEVICE-NAME}" \
        op monitor interval="15s" role="Master" timeout="20" \
        op monitor interval="20s" role="Slave" timeout="20" \
        op start interval="0" timeout="240" \
        op stop interval="0" timeout="100"

crm configure ms ms_drbd_${DRBD-DEVICE-NAME} drbd_${DRBD-DEVICE-NAME} meta notify=true
