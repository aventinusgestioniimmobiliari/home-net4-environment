## Konsole

### Werkzeuge und Tools

* [tmuxcheatsheet](https://tmuxcheatsheet.com/)
* [cool-retro-term](https://github.com/Swordfish90/cool-retro-term )
* [Bookmark für die Konsole](https://itsfoss.com/buku-command-line-bookmark-manager-linux/)
* [no-more-secrets](https://github.com/bartobri/no-more-secrets)
* [terminix](https://github.com/gnunn1/terminix)
* [cmatrix](http://www.asty.org/cmatrix/)
* [termite](https://github.com/thestinger/termite)
* [terminix-web](https://gnunn1.github.io/terminix-web/)
* [TermKit](https://github.com/unconed/TermKit)
* [Xiki makes the shell console more friendly and powerful](http://xiki.org/ )

Install cmatrix
===============
```sudo apt-get install cmatrix```

buku-command-line-bookmark-manager-linux
========================================
```
cd /tmp
git clone https://github.com/jarun/Buku.git
cd Buku
sudo make install
cd auto-completion/bash/
sudo cp buku-completion.bash /etc/bash_completion.d/
```

Features & Basic Usage

Buku supports all the basic bookmarking tasks, like – add, delete, tag, comment, search, update etc. And also adds some extra features on top of that. You can check the Buku manual page for various examples:

man buku

Here are some of the cherry-on-top features of Buku:

Bookmarks Title Fetching: Buku can automatically fetches bookmark title from the web. And you can refresh all the of them with a single command from terminal:

buku --update

Encryption: One of Buku’s interesting features is its built-in encryption mechanism. You can encrypt you bookmark database using this command:

buku --lock

Enter a password to encrypt the database with and it’ll be encrypted using AES 256 bit encryption algorithm. If you don’t want anyone else to peek through your bookmarks, Buku will be of great help. And for decrypting your database simply run:

buku --unlock

Import/Export: Buku can import bookmarks from exported Firefox/Chrome/IE HTML file and also export bookmarks in the same format.

Merge-able Database: Buku’s databases are completely marge-able and portable. So, if you have multiple devices and use Buku on them for bookmark managing, you can grab the database file from one device, merge it with the database on another device and transfer it back and forth!


Tools zum erstellen von tabellen
================================

Quellen
* http://www.tablesgenerator.com/html_tables


Planung
======

Im Verzeichnis contrib werden immer codes für die weitere entwicklung abgelegt.


Verweise
========

* https://github.com/kernt/linuxtools/tree/master/programming/python # ssh script zum installiren 

