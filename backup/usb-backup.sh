#!/bin/bash
#
SOURCES=(/root /etc /home /boot )
TARGET="/media/usb/zielverzeichnis"

# edit or comment with "#"
#LISTPACKAGES=listdebianpackages        # local-mode and tossh-mode
MONTHROTATE=monthrotate                 # use DD instead of YYMMDD

RSYNCCONF=(--delete)
#MOUNTPOINT="/media/daten"               # check local mountpoint
#MAILREC="user@domain"

#SSHUSER="sshuser"
#FROMSSH="fromssh-server"
#TOSSH="tossh-server"
SSHPORT=22
