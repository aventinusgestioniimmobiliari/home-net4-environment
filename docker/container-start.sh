#!/bin/bash
#
#
#
#
# for each docker container add one dir under ~/docker/$CONTAINER
#
# function to test the dir exists
#
if [ -d ~/docker/portainer/data  ]; then
    mkdir -p ~/docker/portainer/data
fi

# https://github.com/kernt/linuxtools/blob/master/bash/conf_slim_down.sh
# most usible in Unix/Linux
grep -Ev '^(#|$)'
 # better for Windows for example opsi files
  # grep -Ev '^(#|;)'

# Source : https://portainer.readthedocs.io/en/latest/deployment.html#connect-to-a-docker-engine-with-tls-enabled
#          https://portainer.readthedocs.io/en/latest/templates.html

if []; then
    docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v /home/tobkern/docker/portainer/data:/data portainer/portainer
fi


