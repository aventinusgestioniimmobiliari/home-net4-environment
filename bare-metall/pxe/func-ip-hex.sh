#!/bin/bash

i2h() {
  if echo $@ | grep -q "\.";
  then 
    printf '%.2X%.2X%.2X%.2X\n' `echo $@ | sed -e 's/\./ /g'`
  else 
    printf '%d.%d.%d.%d\n' `echo $@ | sed -r 's/(..)/0x\1 /g'`
  fi
}

i2h 192.168.4.1
