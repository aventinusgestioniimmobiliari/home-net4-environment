#!/bin/bash
#
#
#
# Source: http://www.lucainvernizzi.net/blog/2014/12/03/vagrant-and-libvirt-kvm-qemu-setting-up-boxes-the-easy-way/
#########################################################################

echo "Installation von vagrant mit "
wget https://releases.hashicorp.com/vagrant/1.9.1/vagrant_1.9.1_x86_64.deb
dpkg -i vagrant_1.9.1_x86_64.deb

apt-get build-dep vagrant ruby-libvirt

apt-get install qemu libvirt-bin ebtables dnsmasq

apt-get install libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev